from time import time
n = int(input())
string = input()
start = time()
revers = string[::-1]

char = string[-1]

iterator = 0
while iterator != n:
    find_iterator = string.find(char, iterator)
    if find_iterator != -1:
        iterator = find_iterator
    else:
        break
    if string[iterator:] + string[0:iterator] == revers:
        print(iterator)
        print(start)
        exit(0)
    iterator += 1

print(-1)

