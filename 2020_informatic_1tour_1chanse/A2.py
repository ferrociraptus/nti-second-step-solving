def to_max(number: float) -> int:
    if number % 1 != 0.0:
        return int((number) + 1)
    return int(number)


input_data = list(map(int, input().split()))
credits_for_win = input_data[0]
max_base_credits = input_data[1]
storage_cost = input_data[2]
max_credits_in_storage = input_data[3]
turn_time = input_data[4]
turn_profit = input_data[5]
army_cost = input_data[6]
enemy = input_data[7]
time_go_to_enemy = input_data[8]

# first_way
count_of_need_storages = to_max((credits_for_win - max_base_credits) / max_credits_in_storage)
if count_of_need_storages < 0:
    sum_of_win_time_case1 = to_max(credits_for_win / turn_profit)*turn_time
else:
    sum_of_win_time_case1 = to_max((credits_for_win + count_of_need_storages * storage_cost) / turn_profit) * turn_time

# second_way
army_at_turn = turn_profit / army_cost
sum_of_win_time_case2 = to_max((enemy + 1)/army_at_turn) * turn_time + time_go_to_enemy

print(min(sum_of_win_time_case1, sum_of_win_time_case2))
