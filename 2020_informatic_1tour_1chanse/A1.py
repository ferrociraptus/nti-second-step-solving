from math import ceil

input_data = list(map(int, input().split()))
credits_for_win = input_data[0]
max_base_credits = input_data[1]
storage_cost = input_data[2]
max_credits_in_storage = input_data[3]
turn_time = input_data[4]
turn_profit = input_data[5]
army_cost = input_data[6]
enemy = input_data[7]
time_go_to_enemy = input_data[8]

# first_way
credit_sum = 0
sum_of_win_time_case1 = 0
max_credits = max_base_credits
while credit_sum < credits_for_win:
    sum_of_win_time_case1 += turn_time
    credit_sum += turn_profit
    while credit_sum > max_credits and max_credits < credits_for_win:
        if credit_sum - storage_cost >= 0:
            credit_sum -= storage_cost
            max_credits += max_credits_in_storage
        else:
            break

# second_way

credit_sum = 0
sum_of_win_time_case2 = 0
max_credits = max_base_credits
army = 0

army_at_turn = turn_profit // army_cost

turn_profit -= army_cost * army_at_turn


time_without_upgrade_max_credit = None
if max_credits < army_cost:
    time_without_upgrade_max_credit = ceil((enemy + 1) / army_at_turn)

while enemy >= army:
    sum_of_win_time_case2 += turn_time
    army += army_at_turn
    credit_sum += turn_profit

    while credit_sum >= army_cost:
        army += 1
        credit_sum -= army_cost

    while credit_sum > max_credits:
        if credit_sum - storage_cost >= 0:
            credit_sum -= storage_cost
            max_credits += max_credits_in_storage
        else:
            break

sum_of_win_time_case2 += time_go_to_enemy

minimum = min(sum_of_win_time_case1, sum_of_win_time_case2)
if time_without_upgrade_max_credit is not None and time_without_upgrade_max_credit < minimum:
    print(time_without_upgrade_max_credit)
else:
    print(minimum)
