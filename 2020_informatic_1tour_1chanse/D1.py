n, m = map(int, input().split())

pairs_lost = {}
for i in range(0, m):
    first, second = map(int, input().split())
    if second not in pairs_lost.keys():
        pairs_lost[second] = []
    if first not in pairs_lost.keys():
        pairs_lost[first] = []
    if first not in pairs_lost[second]:
        pairs_lost[second].append(first)

liar = False
triples = []
for i in range(0, n):
    triples.append(list(map(int, input().split())))

for triple in triples:
    for numb in range(2, 1, -1):
        for i in triple[0:numb]:
            if i not in pairs_lost[triple[numb]]:
                print('liar')
                liar = True
                break
    if not liar:
        print('honest')
    liar = False
