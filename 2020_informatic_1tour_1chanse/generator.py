from random import randint

string = ''
for i in range(0, 3*(10**6)):
    string += chr(randint(ord('a'), ord('r')))

print(string)
print(len(string))