n, x = map(int, input().split())
a = input()

if x < 0:
    c = -x
    x = 0
else:
    c = 0

for i in range(n):
    if a[i] == 'C':
        if not x:
            if i != n - 1:
                c += 1
        else:
            x -= 1
    elif a[i] == 'A':
        x += 1

print(c)