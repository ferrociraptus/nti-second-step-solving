from math import floor, log

n, a, b, maximum = list(map(int, input().split()))
if b != 1:
    max_dishes = floor(log(1 - int(maximum * (1 - b)) // a, b))
else:
    max_dishes = maximum // a

if max_dishes > n:
    print(n)
else:
    print(max_dishes)