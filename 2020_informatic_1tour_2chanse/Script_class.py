from subprocess import Popen, PIPE
from time import time
from typing import Any


class Script:
    def __init__(self, program_path: str, max_running_time: float = None) -> None:
        """
        :param program_path: string pas of program
        :param max_running_time: in ms
        """
        self.program_path = program_path
        self.max_running_time = max_running_time
        self.working_time = None

    def run_script(self, *args, max_running_time: float = None) -> Any:
        """
        simulate script running for testing program from stdin -> stdout
        :param max_running_time: in ms
        :param args: list of args for program
        :return: any output from stdout
        """
        if max_running_time is None:
            max_running_time = self.max_running_time
        self.working_time = None
        start_time = time()
        script = Popen("python " + self.program_path, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        for arg in args:
            script.stdin.write((str(arg) + '\n').encode())
            script.stdin.flush()

        if max_running_time is not None:
            script.wait(max_running_time * 1.5)
        else:
            script.wait()
        self.working_time = time() - start_time
        result = script.communicate()
        if max_running_time is not None:
            if max_running_time <= self.working_time:
                raise TimeoutError("Max working time: {max}, Program working time: {time}".
                                   format(max=max_running_time, time=self.working_time))
        if script.returncode == 0:
            return result[0].decode().strip()
        else:
            raise Exception("Script running error. Exit code:{}".format(script.returncode))
