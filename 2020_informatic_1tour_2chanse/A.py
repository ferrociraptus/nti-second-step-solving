def to_max(number: float) -> int:
    if number % 1 != 0.0:
        return int(number + 1)
    return int(number)


ne = list(map(int, input().split()))
elec = list(map(int, input().split()))
print(to_max(sum(elec) / ne[1]))