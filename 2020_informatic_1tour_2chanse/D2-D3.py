n, m, t = list(map(int, input().split()))
# need_buildings_types = list(input().split())
answer = list(input().split())

buildings = {}
for i in range(0, n):
    b_type = input()
    need_types = list(input().split())
    buildings[b_type] = need_types[1:]

values = answer.copy()
was = dict.fromkeys(answer, True)
# print(answer)

builds = 0
stay_on_place_flag = False
# for build in values:
while builds < len(answer):
    for need_build in buildings[answer[builds]]:
        if not was.get(need_build, False):
            answer.insert(builds + 1, need_build)
            was[need_build] = True
        else:
            if need_build in answer[:builds+1]:
                stay_on_place_flag = True
            answer.remove(need_build)
            if stay_on_place_flag:
                answer.insert(builds, need_build)
            else:
                answer.insert(builds + 1, need_build)

    if not stay_on_place_flag:
        builds += 1
    stay_on_place_flag = False
    # print(answer)

if t == 1:
    print(len(answer))
elif t == 2:
    print(len(answer))
    print(' '.join(answer[::-1]))