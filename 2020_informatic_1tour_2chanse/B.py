n = int(input())
a = list(map(int, input().split()))
index = 0
for i in range(1, n):
    if a[i] < a[i - 1]:
        index += 1
print(index)
