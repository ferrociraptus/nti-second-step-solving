from typing import List, Dict
# from time import time


def need_buildings(buildings: Dict, need_to_build, answer: List, const):

    if len(buildings[need_to_build]) == 0:
        return [need_to_build]
    answer = [need_to_build] + answer
    for build_type in buildings[need_to_build]:
        new = need_buildings(buildings, build_type, answer, build_type)
        answer = new + answer

    if const == need_to_build:
        return answer

    if const == need_to_build:
        return answer

    return new + [need_to_build]


n, m, t = list(map(int, input().split()))
need_build_types = list(input().split())

buildings = {}
for i in range(0, n):
    b_type = input()
    need_types = list(input().split())
    buildings[b_type] = need_types[1:]

# start = time()
answer = []
for need_to_build in need_build_types:
    answer = need_buildings(buildings, need_to_build, [], need_to_build) + answer
ans = []

for el in answer:
    if el not in ans:
        ans.append(el)

if t == 1:
    print(len(ans))
elif t == 2:
    print(len(ans))
    print(ans)

