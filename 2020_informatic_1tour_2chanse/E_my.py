
class Rect:
    def __init__(self, x1, y1, x2, y2, index):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.index = index

    def is_point_into(self, point):
        if self.x1 <= point[0] <= self.x2 and self.y1 <= point[1] <= self.y2:
            return True
        else:
            return False

bottles_count, points = list(map(int, input().split()))


bottles = []

result = []

flag_ok = False
loose_points = {}
loose_bottls = {}

for i in range(bottles_count):
    new_bottle = list(map(int, input().split()))
    bottles.append(Rect(new_bottle[0],new_bottle[1], new_bottle[2], new_bottle[3], i + 1))

for i in range(points):
    point = tuple(map(int, input().split()))
    if not loose_points.get(point, False):
        for bottle in range(len(bottles)):
            if not loose_bottls.get(bottle, False):
                if bottles[bottle].is_point_into(point):
                    result.append(str(bottles[bottle].index))
                    # bottles.pop(bottle)
                    loose_bottls[bottle] = True
                    flag_ok = True
                    break
        if not flag_ok:
            result.append('-1')

        if len(bottles) <= 0:
            print((' '.join(result) + ' -1' * (points - len(result))))
            exit(0)

        loose_points[point] = True
        flag_ok = False
    else:
        result.append('-1')

print(' '.join(result))
