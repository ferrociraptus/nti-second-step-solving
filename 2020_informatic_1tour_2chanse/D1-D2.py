# from time import time
n, m, t = list(map(int, input().split()))
answer = list(input().split())

buildings = {}
for i in range(0, n):
    b_type = input()
    need_types = list(input().split())
    buildings[b_type] = need_types[1:]

ans = dict.fromkeys(answer)

for build in answer:
    for need_build in buildings[build]:
        # if need_build not in answer:
        if not ans.get(need_build, False):
            answer.append(need_build)
            ans[need_build] = True

print(len(ans))
