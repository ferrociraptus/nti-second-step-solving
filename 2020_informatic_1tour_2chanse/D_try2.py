from time import time
n, m, t = list(map(int, input().split()))
# need_buildings_types = list(input().split())
answer = list(input().split())

buildings = {}
for i in range(0, n):
    b_type = input()
    need_types = list(input().split())
    buildings[b_type] = need_types[1:]
start = time()
values = answer.copy()
# print(answer)
for build in values:
    for need_build in range(0, len(buildings[build])):
        if need_build not in answer:
            answer.append(need_build)
            values.append(need_build)
        else:
            answer.remove(need_build)
            answer.append(need_build)
    # print(answer)

if t == 1:
    print(len(answer))
elif t == 2:
    print(len(answer))
    print(' '.join(answer[::-1]))
print(time()-start)