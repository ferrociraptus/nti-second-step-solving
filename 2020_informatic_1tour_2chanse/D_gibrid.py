from typing import List, Dict
# from time import time


def need_buildings(buildings: Dict, need_to_build, answer: List, const):

    if len(buildings[need_to_build]) == 0:
        return [need_to_build]
    if need_to_build not in answer:
        answer = [need_to_build] + answer
    for build_type in buildings[need_to_build]:
        if build_type not in answer:
            new = need_buildings(buildings, build_type, answer, build_type)
            for n in new[::-1]:
                if n not in answer:
                    answer = [n] + answer
                else:
                    answer.remove(n)
                    answer = [n] + answer

    if const == need_to_build:
        return answer

    if const == need_to_build:
        return answer

    return new + [need_to_build]


n, m, t = list(map(int, input().split()))
need_build_types = list(input().split())

answer = need_build_types[::-1].copy()

buildings = {}
for i in range(0, n):
    b_type = input()
    need_types = list(input().split())[1:]
    buildings[b_type] = need_types
    if b_type in answer:
        for n_type in need_types:
            if n_type not in answer:
                answer = [n_type] + answer
            else:
                answer.remove(n_type)
                answer = [n_type] + answer

# start = time()
for need_type in answer:
    answer = need_buildings(buildings, need_type, answer, need_type)

if t == 1:
    print(len(answer))
elif t == 2:
    print(len(answer))
    print(answer)

# print(f'Time: {time() - start}')