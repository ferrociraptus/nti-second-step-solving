import unittest
from typing import Any
from random import randint

from Script_class import Script


class MyTestCase(unittest.TestCase):
    def assertEqual(self, first: Any, second: Any, msg: Any = ...) -> None:
        first = str(first)
        second = str(second)
        super().assertEqual(first, second, msg)

    def setUp(self) -> None:
        self.script = Script("/home/arseniy/Documents/Python/NTI_2019/2020_informatic_1tour_2chanse/D2-D3.py", 3000)

    def test_Standard_test(self):
        res = self.script.run_script('3 1 1', '1', '2', '0', '1', '2 2 3', '3', '0')
        self.assertEqual(res, 3)
