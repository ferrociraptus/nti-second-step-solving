n = int(input())
lstr = input().split(" ")
ns = []
i = 0
tot = 0
while i < n:
    ns.append(int(lstr[i]))
    tot += int(lstr[i])
    i += 1
if tot % 5 == 0 and len(ns) > 4:
    sm = int(tot / 5)
    ns = sorted(ns, reverse=True)
    ps = "Yes"
    su = 0
    sk = 0
    b = False
    for n in ns:
        if n != 0 and b:
            su == 0
            sk += 1
            b = False
            break
        if sk == 5:
            ps = "No"
            break
        su += n
        if abs(su) > abs(sm):
            ps = "No"
            break
        if su == sm:
            b = True
    print(ps)
else:
    print("No")